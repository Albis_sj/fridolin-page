import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contactanos',
  templateUrl: './contactanos.component.html',
  styleUrls: ['./contactanos.component.css']
})
export class ContactanosComponent implements OnInit {

  formulario!: FormGroup;
  mensaje!: string;
  agradecimiento: string = 'GRACIAS POR COMUNICARSE CON NOSOTROS';

  constructor(private fb: FormBuilder,) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]]
    });
  }

  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched
  }

  get comentarioNoValido(){
    return this.formulario.get('comentario')?.invalid && this.formulario.get('comentario')?.touched
  }

  get telefonoNoValido(){
    return this.formulario.get('telefono')?.invalid && this.formulario.get('telefono')?.touched
  }
  ngOnInit(): void {
  }

  guardar(): void{
    console.log(this.formulario);
    this.LimpiarFormulario()
    this.MensajeEnviado()
  }

  LimpiarFormulario(){
    this.formulario.reset();
  }

  MensajeEnviado(){
    this.mensaje = this.agradecimiento
  }

}
