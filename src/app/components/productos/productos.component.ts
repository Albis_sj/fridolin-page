import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService, Producto } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Producto[] = [];

  constructor(private _dataService: DataService,
              private router: Router) { 
    console.log('constructor');
    
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.productos = this._dataService.getProductos();
    console.log(this.productos);
  }

  verProducto(idx: number): void {
    console.log(idx);
    this.router.navigate(['/heroe', idx]);    
  }

}
