import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {

  fecha!: Date;
  anio!: number;

  constructor() { 
    this.fecha = new Date();
    this.anio = this.fecha.getFullYear();
  }

  ngOnInit(): void {
  }

}
