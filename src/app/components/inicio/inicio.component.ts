import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  meses: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
  fecha!: Date;
  mesNumber!: number;
  mes!: string;

  constructor() { 
    this.fecha = new Date();
    this.mesNumber = this.fecha.getMonth();
    this.mes = this.meses[this.mesNumber]
  }

  ngOnInit(): void {
  }

}
