import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  producto: any = {};

  constructor(private activatedRoute: ActivatedRoute,
              private _dataService: DataService) { 
    this.activatedRoute.params.subscribe(params => {
      console.log(params);
      console.log(params['id']);
      
      this.producto = this._dataService.getProducto(params['id'])
    })
  }

  ngOnInit(): void {
  }

}
