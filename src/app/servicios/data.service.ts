import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private productos: Producto[] = [
    {
      img: "assets/img/producto/marshmellow.png",
      nombre: "Torta Marshmellow",
      precio: "Bs.99.00",
    },
    {
      img: "assets/img/producto/tres_leches.png",
      nombre: "Torta Rosa Tres Leches 12 pers.s",
      precio: "Bs.270.00",
    },
    {
      img: "assets/img/producto/mini-tortas.jpg",
      nombre: "Mini Torta",
      precio: "Bs.19.00",
    },
    {
      img: "assets/img/producto/pack_salado.jpg",
      nombre: "Party Pack Mediano",
      precio: "Bs.99.00",
    },
    {
      img: "assets/img/producto/desayuno.jpg",
      nombre: "Desayuno Fridolin",
      precio: "Bs.270.00",
    },
    {
      img: "assets/img/producto/torta_moka.png",
      nombre: "Torta Moka 12 pers.",
      precio: "Bs.145.00",
    },
    {
      img: "assets/img/producto/torta_rosa_negra.png",
      nombre: "Torta Rosa Negra 12 pers.",
      precio: "Bs.175.00",
    },
    {
      img: "assets/img/producto/torta_maracuya.png",
      nombre: "Torta de MAracuyá & Chocolate 12 pers.",
      precio: "Bs.155.00",
    }
  ];

  getProductos(): any[] {
    return this.productos
  }

  constructor() { 
    console.log('servicio listo');
  }

  getProducto(idx: number): Producto{
    return this.productos[idx]
  }
}


export interface Producto { //el export hace que lo podamos usar en todo el proyecto
  img: string;
  nombre: string;
  precio: String;
  //colocamos el valor idx como opcional
  idx?: number;
}
